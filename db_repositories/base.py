"""Module with base classes for repositories."""
import databases
from fastapi import Depends

from dialogs import connections


class BaseRepository:
    """Basic repository."""

    def __init__(self, database_connection: connections.DatabaseConnection = Depends(connections.DatabaseConnection)):
        self.database_connection: databases.Database = database_connection.connection
