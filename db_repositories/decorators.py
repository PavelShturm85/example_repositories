"""Database-specific decorators."""
import functools

import asyncpg
import backoff

from dialogs import settings


def postgres_reconnect(func):
    """Decorator for postgres reconnection."""

    @backoff.on_exception(
        backoff.expo, (asyncpg.PostgresConnectionError,), max_tries=settings.POSTGRES_CONNECTION_TRIES,
    )
    @functools.wraps(func)
    async def wrapped_method(*args, **kwargs):
        return await func(*args, **kwargs)

    return wrapped_method
