"""Messages repositories."""
import datetime
import typing

import asyncpg
from loguru import logger

from dialogs.models import entities as models
from dialogs.repositories.base import BaseRepository
from dialogs.repositories.decorators import postgres_reconnect


class MessagesRepository(BaseRepository):
    """Messages repository."""

    @postgres_reconnect
    async def fetch_messages_list_by_user_id(
        self, public_user_id: str, start_message_id: typing.Optional[int], messages_count: int
    ) -> typing.List[models.Message]:
        """Fetch messages list by user id with pagination."""
        if start_message_id:
            mapping_result_list: typing.List[typing.Mapping] = await self.database_connection.fetch_all(
                '''
                SELECT id, dialog_id, cite_id, public_user_id, private_user_id, changed, delivered,
                       direction, message_type, is_delivered, is_readed, is_failed, status, message_text, rich_media,
                       external_message_id, idempotency_id, author_info
                  FROM messages
                  WHERE public_user_id=:public_user_id AND id<=:start_message_id
                  ORDER BY id DESC
                  LIMIT :messages_count;
                ''',
                {
                    'public_user_id': public_user_id,
                    'start_message_id': start_message_id,
                    'messages_count': messages_count + 1,
                },
            )
        else:
            mapping_result_list = await self.database_connection.fetch_all(
                '''
                SELECT id, dialog_id, cite_id, public_user_id, private_user_id, changed,
                       delivered, direction, message_type, is_delivered, is_readed, is_failed, status, message_text,
                       rich_media, external_message_id, idempotency_id, author_info
                  FROM messages
                  WHERE public_user_id=:public_user_id
                  ORDER BY id DESC
                  LIMIT :messages_count;
                ''',
                {'public_user_id': public_user_id, 'messages_count': messages_count + 1},
            )
        return [models.Message(**mapping_result) for mapping_result in mapping_result_list]

    @postgres_reconnect
    async def fetch_one_message_by_user_id(
        self, public_user_id: str, message_id: int
    ) -> typing.Optional[models.Message]:
        """Fetch one message by user id."""
        mapping_result: typing.Optional[typing.Mapping] = await self.database_connection.fetch_one(
            '''
            SELECT id, dialog_id, cite_id, public_user_id, private_user_id, changed, delivered, direction, message_type,
                   is_delivered, is_readed, is_failed, status, message_text, rich_media, external_message_id,
                   idempotency_id, author_info
             FROM messages
             WHERE public_user_id=:public_user_id AND id=:message_id
             LIMIT 1;
            ''',
            {'public_user_id': public_user_id, 'message_id': message_id},
        )
        if not mapping_result:
            return None
        return models.Message(**mapping_result)

    @postgres_reconnect
    async def fetch_one_message_by_id(self, message_id: int) -> typing.Optional[models.Message]:
        """Fetch one message by user id."""
        mapping_result: typing.Optional[typing.Mapping] = await self.database_connection.fetch_one(
            '''
            SELECT id, dialog_id, cite_id, public_user_id, private_user_id, changed, delivered, direction, message_type,
                   is_delivered, is_readed, is_failed, status, message_text, rich_media, external_message_id,
                   idempotency_id, author_info
             FROM messages
             WHERE id=:message_id
             LIMIT 1;
            ''',
            {'message_id': message_id},
        )
        if not mapping_result:
            return None
        return models.Message(**mapping_result)

    @postgres_reconnect
    async def create_message(self, message: models.Message) -> typing.Optional[models.Message]:
        """Create message in database."""
        mapping_result: typing.Optional[typing.Mapping] = await self.database_connection.fetch_one(
            '''
                INSERT INTO messages(
                    dialog_id, cite_id, public_user_id, private_user_id, changed, delivered, direction,
                    external_message_id, message_type, is_delivered, is_readed, is_failed, status,
                    message_text, rich_media, idempotency_id, author_info)
                VALUES (
                    :dialog_id, :cite_id, :public_user_id, :private_user_id, :changed, :delivered, :direction,
                    :external_message_id, :message_type, :is_delivered, :is_readed, :is_failed, :status,
                    :message_text, :rich_media, :idempotency_id, :author_info)
                RETURNING id, dialog_id, cite_id, public_user_id, private_user_id, changed, delivered, direction,
                          message_type, is_delivered, is_readed, is_failed, status, message_text, rich_media,
                          external_message_id, idempotency_id, author_info;
            ''',
            values=message.dict(exclude={'id'}),
        )
        if not mapping_result:
            return None
        return models.Message(**mapping_result)

    @postgres_reconnect
    async def add_readed_status_to_message_by_private_user(
        self, internal_message_id: int, dialog_id: int
    ) -> typing.Union[typing.List[models.Message], typing.List]:
        """Add readed status to message by private user."""
        mapping_result_list: typing.List[typing.Mapping] = await self.database_connection.fetch_all(
            '''
            UPDATE messages
                SET is_readed=:is_readed
             WHERE dialog_id=:dialog_id AND is_readed=:not_read AND id<=:id AND private_user_id IS NULL
             RETURNING id, dialog_id, cite_id, public_user_id, private_user_id, changed, delivered, direction,
                       message_type, is_delivered, is_readed, is_failed, status, message_text, rich_media,
                       external_message_id, idempotency_id, author_info;
            ''',
            {'id': internal_message_id, 'is_readed': True, 'not_read': False, 'dialog_id': dialog_id},
        )
        return [models.Message(**mapping_result) for mapping_result in mapping_result_list]

    @postgres_reconnect
    async def add_readed_status_to_message_by_public_user(
        self, internal_message_id: int, dialog_id: int
    ) -> typing.Union[typing.List[models.Message], typing.List]:
        """Add readed status to message by public user."""
        mapping_result_list: typing.List[typing.Mapping] = await self.database_connection.fetch_all(
            '''
            UPDATE messages
                SET is_readed=:is_readed
             WHERE dialog_id=:dialog_id AND is_failed=:not_failed AND is_readed=:not_read
                   AND id=:id AND private_user_id IS NOT NULL
             RETURNING id, dialog_id, cite_id, public_user_id, private_user_id, changed, delivered, direction,
                       message_type, is_delivered, is_readed, is_failed, status, message_text, rich_media,
                       external_message_id, idempotency_id, author_info;
            ''',
            {
                'id': internal_message_id,
                'is_readed': True,
                'not_read': False,
                'not_failed': False,
                'dialog_id': dialog_id,
            },
        )
        return [models.Message(**mapping_result) for mapping_result in mapping_result_list]

    @postgres_reconnect
    async def add_delivered_status_to_message(
        self, internal_message_id: int, delivered: datetime.datetime
    ) -> typing.Optional[models.Message]:
        """Add delivered status to message."""
        mapping_result: typing.Optional[typing.Mapping] = await self.database_connection.fetch_one(
            '''
            UPDATE messages
               SET is_delivered=:is_delivered, delivered=:delivered
             WHERE id=:id
             RETURNING id, dialog_id, cite_id, public_user_id, private_user_id, changed, delivered, direction,
                       message_type, is_delivered, is_readed, is_failed, status, message_text, rich_media,
                       external_message_id, idempotency_id, author_info;
            ''',
            {'id': internal_message_id, 'is_delivered': True, 'delivered': delivered},
        )
        if not mapping_result:
            return None
        return models.Message(**mapping_result)

    @postgres_reconnect
    async def add_deleted_status_to_message(self, internal_message_id: int) -> typing.Optional[models.Message]:
        """Add delete status to message."""
        mapping_result: typing.Optional[typing.Mapping] = await self.database_connection.fetch_one(
            '''
            UPDATE messages
               SET status=:status
             WHERE id=:id
             RETURNING id, dialog_id, cite_id, public_user_id, private_user_id, changed, delivered, direction,
                       message_type, is_delivered, is_readed, is_failed, status, message_text, rich_media,
                       external_message_id, idempotency_id, author_info;
            ''',
            {'id': internal_message_id, 'status': models.MessageStatusEnum.DELETED},
        )
        if not mapping_result:
            return None
        return models.Message(**mapping_result)

    @postgres_reconnect
    async def add_failed_status_to_message(self, internal_message_id: int) -> typing.Optional[models.Message]:
        """Add failed status to message."""
        mapping_result: typing.Optional[typing.Mapping] = await self.database_connection.fetch_one(
            '''
            UPDATE messages
               SET is_failed=:is_failed
             WHERE id=:id
             RETURNING id, dialog_id, cite_id, public_user_id, private_user_id, changed, delivered, direction,
                       message_type, is_delivered, is_readed, is_failed, status, message_text, rich_media,
                       external_message_id, idempotency_id, author_info;
            ''',
            {'id': internal_message_id, 'is_failed': True},
        )
        if not mapping_result:
            return None
        return models.Message(**mapping_result)

    @postgres_reconnect
    async def update_message(
        self, message_id: int, message_text: str, time_now: datetime.datetime
    ) -> typing.Optional[models.Message]:
        """Transaction update message."""
        try:
            async with self.database_connection.transaction():
                previous_message: typing.Optional[models.Message] = await self.fetch_one_message_by_id(message_id)
                if not previous_message:
                    return None

                new_message: typing.Optional[typing.Mapping] = await self.database_connection.fetch_one(
                    '''
                    UPDATE messages
                       SET message_text=:message_text, status=:status
                     WHERE id=:id
                     RETURNING id, dialog_id, cite_id, public_user_id, private_user_id, changed, delivered, direction,
                               message_type, is_delivered, is_readed, is_failed, status, message_text, rich_media,
                               external_message_id, idempotency_id, author_info;
                    ''',
                    {'id': message_id, 'message_text': message_text, 'status': models.MessageStatusEnum.EDITED},
                )
                if not new_message:
                    return None

                await self.database_connection.execute(
                    '''
                    INSERT INTO messages_history(
                        message_id, previous_message_text, changed, private_user_id, public_user_id)
                      VALUES (:message_id, :previous_message_text, :changed, :private_user_id, :public_user_id);
                    ''',
                    {
                        'message_id': message_id,
                        'previous_message_text': previous_message.message_text,
                        'changed': time_now,
                        'public_user_id': previous_message.public_user_id,
                        'private_user_id': previous_message.private_user_id,
                    },
                )
                return models.Message(**new_message)
        except asyncpg.PostgresError:
            logger.exception(f'Transaction update fail. Can not update message, id: {message_id}')
            return None
