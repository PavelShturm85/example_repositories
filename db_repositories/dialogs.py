"""Dialogs repositories."""
import datetime
import typing

import sqlalchemy as sa

from dialogs.models import dialogs as dialogs_models
from dialogs.models import entities as models
from dialogs.repositories.base import BaseRepository
from dialogs.repositories.decorators import postgres_reconnect


class DialogsRepository(BaseRepository):
    """Dialogs repository."""

    # pylint: disable=too-many-arguments
    @postgres_reconnect
    async def fetch_all_dialogs_with_filters(
        self,
        from_create_date: typing.Optional[datetime.datetime] = None,
        to_create_date: typing.Optional[datetime.datetime] = None,
        channel_id: typing.Optional[int] = None,
        skill_id: typing.Optional[int] = None,
        public_user_id: typing.Optional[str] = None,
    ) -> typing.List[models.Dialog]:
        """Fetch all dialogs with filters."""
        query = sa.select(
            [
                sa.column('id'),
                sa.column('ip_addr'),
                sa.column('operator_id'),
                sa.column('started'),
                sa.column('changed'),
                sa.column('closed'),
                sa.column('channel_id'),
                sa.column('skill_id'),
                sa.column('public_user_id'),
                sa.column('status'),
                sa.column('idempotency_id'),
            ]
        ).select_from(sa.table('dialogs'))
        if from_create_date:
            query = query.where(sa.column('started') >= from_create_date)
        if to_create_date:
            query = query.where(sa.column('started') <= to_create_date)
        if channel_id:
            query = query.where(sa.column('channel_id') == channel_id)
        if skill_id:
            query = query.where(sa.column('skill_id') == skill_id)
        if public_user_id:
            query = query.where(sa.column('public_user_id') == public_user_id)
        mapping_result_list: typing.List[typing.Mapping] = await self.database_connection.fetch_all(query)
        return [models.Dialog(**mapping_result) for mapping_result in mapping_result_list]

    @postgres_reconnect
    async def fetch_dialog_by_id(self, dialog_id: int) -> typing.Optional[models.Dialog]:
        """Fetch dialog by id."""
        mapping_result: typing.Optional[typing.Mapping] = await self.database_connection.fetch_one(
            '''
            SELECT id, ip_addr, operator_id, started, changed, closed, channel_id, skill_id, public_user_id, status,
                   idempotency_id
              FROM dialogs
             WHERE id=:dialog_id
             LIMIT 1;
            ''',
            {'dialog_id': dialog_id},
        )
        if not mapping_result:
            return None
        return models.Dialog(**mapping_result)

    @postgres_reconnect
    async def fetch_active_user_dialog(self, public_user_id: str) -> typing.Optional[models.Dialog]:
        """Fetch last active user dialog."""
        mapping_result: typing.Optional[typing.Mapping] = await self.database_connection.fetch_one(
            '''
            SELECT id, ip_addr, operator_id, started, changed, closed, channel_id, skill_id, public_user_id, status,
                   idempotency_id
              FROM dialogs
             WHERE public_user_id=:public_user_id AND status!=:status
             ORDER BY started DESC
             LIMIT 1;
            ''',
            {'public_user_id': public_user_id, 'status': models.DialogStatusEnum.CLOSED},
        )
        if not mapping_result:
            return None
        return models.Dialog(**mapping_result)

    @postgres_reconnect
    async def create_dialog(self, dialog: models.Dialog) -> typing.Optional[models.Dialog]:
        """Create dialog."""
        mapping_result: typing.Optional[typing.Mapping] = await self.database_connection.fetch_one(
            '''
                INSERT INTO dialogs(
                    ip_addr, operator_id, started, changed, closed, channel_id, skill_id, public_user_id, status,
                    idempotency_id
                )
                VALUES (
                    :ip_addr, :operator_id, :started, :changed, :closed, :channel_id,  :skill_id, :public_user_id,
                    :status, :idempotency_id)
                RETURNING id, ip_addr, operator_id, started, changed,
                          closed, channel_id, skill_id, public_user_id, status, idempotency_id;
            ''',
            values=dialog.dict(exclude={'id'}),
        )
        if not mapping_result:
            return None
        return models.Dialog(**mapping_result)

    @postgres_reconnect
    async def fetch_operator_id(self, dialog_id: int) -> typing.Optional[int]:
        """Fetch operator id."""
        return await self.database_connection.fetch_val(
            '''
            SELECT operator_id
              FROM dialogs
             WHERE id=:id
             LIMIT 1;
            ''',
            {'id': dialog_id},
            'operator_id',
        )

    @postgres_reconnect
    async def close_dialog(self, dialog_id: int) -> None:
        """Close dialog."""
        await self.database_connection.execute(
            '''
            UPDATE dialogs
              SET status=:status, closed=:closed_datetime
              WHERE id=:dialog_id;
            ''',
            {
                'status': models.DialogStatusEnum.CLOSED,
                'closed_datetime': datetime.datetime.now(),
                'dialog_id': dialog_id,
            },
        )

    @postgres_reconnect
    async def update_dialog(self, message: dialogs_models.DialogLifeStatusMessage, message_status: int) -> bool:
        """Update dialog."""
        mapping_result: typing.Optional[typing.Mapping] = await self.database_connection.execute(
            '''
            UPDATE dialogs
              SET status=:status, changed=:time_stamp
              WHERE id=:dialog_id AND status!=:status
              RETURNING 1;
            ''',
            {
                'status': message_status,
                'time_stamp': message.time_stamp,
                'dialog_id': message.dialog_id,
            },
        )
        return bool(mapping_result)

    @postgres_reconnect
    async def create_dialog_status_history(self, one_dialog: models.Dialog, time_stamp: datetime.datetime) -> None:
        """Create dialog status history."""
        await self.database_connection.execute(
            '''
                INSERT INTO dialogs_status_history(dialog_id, changed, previous_skill_id, previous_status)
                VALUES (:dialog_id, :changed, :previous_skill_id, :previous_status);
            ''',
            {
                'dialog_id': one_dialog.id,
                'changed': time_stamp,
                'previous_skill_id': one_dialog.skill_id,
                'previous_status': one_dialog.status,
            },
        )
