"""Basic decorators for db access."""
import functools

import backoff
from aioredis.errors import ConnectionClosedError, MultiExecError

from queues import settings


def redis_reconnect(func):
    """Decorator for redis reconnection."""

    @backoff.on_exception(
        backoff.expo, (MultiExecError, ConnectionClosedError), max_tries=settings.REDIS_CONNECTION_TRIES,
    )
    @functools.wraps(func)
    async def wrapped_method(*args, **kwargs):
        return await func(*args, **kwargs)

    return wrapped_method
