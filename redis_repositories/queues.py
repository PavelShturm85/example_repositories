"""External communications logic lies here."""
import copy
import datetime
import json
import typing

import aioredis
from aioredis.commands.transaction import Pipeline
from fastapi import Depends
from loguru import logger

from queues import connections, settings
from queues.models import entities
from queues.models import operator as operator_models
from queues.repositories.decorators import redis_reconnect


class QueuesRepository:
    """Queue repository."""

    def __init__(self, connection: connections.RedisConnection = Depends(connections.RedisConnection)):
        self.connection: aioredis.Redis = connection.get_connection()

    def _prepare_dict_for_redis(self, initial_dict: dict):
        """Convert python type to native redis format."""
        prepared_dict: dict = {}
        for key, value in initial_dict.items():
            if isinstance(value, datetime.datetime):
                prepared_dict[key] = value.timestamp()
            elif isinstance(value, bool):
                prepared_dict[key] = int(value)
            elif isinstance(value, dict):
                prepared_dict[key] = json.dumps(value)
            elif value is None:
                prepared_dict[key] = ''
            else:
                prepared_dict[key] = value
        return prepared_dict

    def _convert_redis_dict_to_python(self, redis_dict: dict):
        converted_dict: dict = {}
        for key, value in redis_dict.items():
            if value == '':
                converted_dict[key] = None
            else:
                converted_dict[key] = value
        return converted_dict

    def _make_dialog_key(self, dialog_id: int):
        return f'{settings.REDIS_DIALOG_PREFIX_KEY}{dialog_id}'

    def _make_operator_queue_key(self, operator_id: str):
        """Make operator key."""
        return f'{settings.REDIS_OPERATOR_QUEUE_PREFIX_KEY}{operator_id}'

    def _make_operator_status_key(self, operator_id: str):
        """Make status key."""
        return f'{settings.REDIS_OPERATOR_STATUS_PREFIX_KEY}{operator_id}'

    def _extract_operator_id_from_status_key(self, status_key: str) -> str:
        return status_key[len(settings.REDIS_OPERATOR_STATUS_PREFIX_KEY) :]

    def _make_operator_status_global_key(self):
        """Make status global key."""
        return f'{settings.REDIS_OPERATOR_STATUS_PREFIX_KEY}*'

    def _pop_dict_item_as_json(self, key: str, dictionary: dict):
        item: str = dictionary.pop(key, '')
        item_dict: dict = {}
        if item:
            item_dict = json.loads(item)
        return item_dict

    def _prepare_list_for_zadd(self, list_to_prepare):
        """Flatten list of tuples to zadd format like:

        [(1, "1"), (2, "2")] -> [1, "1", 2, "2"]
        """
        return list(sum(list_to_prepare, ()))

    @redis_reconnect
    async def fetch_dialog(self, dialog_id: int) -> typing.Optional[entities.QueueDialog]:
        """Fetch dialog."""
        dialog_dict: typing.Optional[dict] = await self.connection.hgetall(
            self._make_dialog_key(dialog_id), encoding='utf-8'
        )
        if not dialog_dict:
            return None
        public_user_info: entities.AuthorInfo = entities.AuthorInfo.parse_obj(
            self._pop_dict_item_as_json('public_user_info', dialog_dict)
        )
        return entities.QueueDialog(
            **self._convert_redis_dict_to_python(dialog_dict),
            public_user_info=public_user_info,
        )

    @redis_reconnect
    async def increment_unread_messages_in_dialog(self, dialog_id: int):
        """Increment unread messages in dialog."""
        await self.connection.hincrby(self._make_dialog_key(dialog_id), 'unread_count')

    @redis_reconnect
    async def decrement_unread_messages_in_dialog(self, dialog_id: int):
        """Decrement unread messages in dialog."""
        # for some reason redis doesn't have a hdecrby function :(
        await self.connection.hincrby(self._make_dialog_key(dialog_id), 'unread_count', increment=-1)

    @redis_reconnect
    async def pop_dialogs_from_general_queue(self, amount: int) -> typing.List[int]:
        """Pop dialogs from general queue."""
        pairs: list = await self.connection.zpopmin(settings.REDIS_GENERAL_QUEUE_KEY, amount, encoding='utf-8')
        # return only dialog ids
        return [int(elem) for index, elem in enumerate(pairs) if index % 2 == 0]

    @redis_reconnect
    async def pop_dialogs_from_personal_queue(
        self, operator_id: str, amount: typing.Optional[int] = None
    ) -> typing.List[int]:
        """Pop dialogs from personal queue."""
        operator_queue_key: str = self._make_operator_queue_key(operator_id)
        if amount:
            pairs: list = await self.connection.zpopmin(operator_queue_key, amount, encoding='utf-8')
            # return only dialog ids
            return [int(elem) for index, elem in enumerate(pairs) if index % 2 == 0]

        dialog_ids: list = await self.connection.zrange(operator_queue_key, encoding='utf-8')
        await self.connection.delete(operator_queue_key)
        return [int(dialog_id) for dialog_id in dialog_ids]

    @redis_reconnect
    async def update_dialog(
        self,
        dialog_id: int,
        updated_fields: dict,
        update_if_not_existed: typing.Optional[set] = None,
        condition: typing.Optional[typing.Coroutine] = None,
    ) -> typing.Optional[entities.QueueDialog]:
        """Update dialog."""
        update_if_not_existed = set() if not update_if_not_existed else update_if_not_existed
        dialog_key: str = self._make_dialog_key(dialog_id)

        with await self.connection as context:
            await context.watch(dialog_key)
            # it's not the best solution but we need to track phantom reads somehow
            if not condition or await condition:
                dialog_dict: typing.Optional[dict] = await context.hgetall(dialog_key, encoding='utf-8')
                if not dialog_dict:
                    dialog_dict = {}

                old_last_message_id: int = dialog_dict.get('last_message_id', 0)
                new_last_message_id: int = updated_fields.get('last_message_id', 0)
                if int(new_last_message_id) < int(old_last_message_id):
                    logger.info(f'Race condition detected, {old_last_message_id} >= {new_last_message_id}')
                    return None

                updated_dialog_dict: dict = self._prepare_dict_for_redis(updated_fields)
                for field_name in update_if_not_existed:
                    if dialog_dict.get(field_name, None):
                        updated_dialog_dict.pop(field_name, None)

                dialog_dict.update(updated_dialog_dict)
                transaction: aioredis.commands.transaction.MultiExec = context.multi_exec()
                transaction.hmset_dict(dialog_key, dialog_dict)
                await transaction.execute()
                public_user_info: entities.AuthorInfo = entities.AuthorInfo.parse_obj(
                    self._pop_dict_item_as_json('public_user_info', dialog_dict)
                )
                return entities.QueueDialog(
                    **self._convert_redis_dict_to_python(dialog_dict), public_user_info=public_user_info
                )

        return None

    @redis_reconnect
    async def remove_dialog_from_personal_queue(self, dialog_id: int, private_user_id: str) -> None:
        """Remove dialog."""
        with await self.connection as context:
            transaction: aioredis.commands.transaction.MultiExec = context.multi_exec()
            transaction.zrem(self._make_operator_queue_key(private_user_id), dialog_id)
            transaction.delete(self._make_dialog_key(dialog_id))
            await transaction.execute()

    @redis_reconnect
    async def copy_dialogs_to_personal_queue(
        self, operator_id: str, dialogs: typing.List[entities.QueueDialog]
    ) -> typing.List[entities.QueueDialog]:
        """Move dialogs to personal queue."""
        if not dialogs:
            return []
        pipeline: Pipeline = self.connection.pipeline()
        pipeline.zadd(
            self._make_operator_queue_key(operator_id),
            *self._prepare_list_for_zadd(
                [(dialog.dialog_created_datetime.timestamp(), dialog.dialog_id) for dialog in dialogs]
            ),
        )
        changed_dialogs: typing.List[entities.QueueDialog] = []
        for dialog in dialogs:
            pipeline.hmset(self._make_dialog_key(dialog.dialog_id), 'in_work', int(False))
            pipeline.hmset(self._make_dialog_key(dialog.dialog_id), 'private_user_id', operator_id)
            pipeline.hmset(
                self._make_dialog_key(dialog.dialog_id),
                'dialog_created_datetime',
                dialog.dialog_created_datetime.timestamp(),
            )
            pipeline.hmset(
                self._make_dialog_key(dialog.dialog_id),
                'dialog_id',
                dialog.dialog_id,
            )
            changed_dialog: entities.QueueDialog = copy.copy(dialog)
            changed_dialog.in_work = False
            changed_dialog.private_user_id = operator_id
            changed_dialogs.append(changed_dialog)

        await pipeline.execute()
        return changed_dialogs

    @redis_reconnect
    async def copy_dialogs_to_general_queue(
        self, dialogs: typing.List[entities.QueueDialog]
    ) -> typing.List[entities.QueueDialog]:
        """Move dialogs to general queue."""
        if not dialogs:
            return []
        with await self.connection as context:
            transaction: aioredis.commands.transaction.MultiExec = context.multi_exec()
            changed_dialogs: typing.List[entities.QueueDialog] = []
            for dialog in dialogs:
                if dialog.private_user_id:
                    await context.watch(self._make_operator_queue_key(dialog.private_user_id))

                transaction.zadd(
                    settings.REDIS_GENERAL_QUEUE_KEY,
                    *self._prepare_list_for_zadd(
                        [(dialog.dialog_created_datetime.timestamp(), dialog.dialog_id) for dialog in dialogs]
                    ),
                )
                transaction.hmset(self._make_dialog_key(dialog.dialog_id), 'in_work', int(False))
                transaction.hmset(self._make_dialog_key(dialog.dialog_id), 'private_user_id', '')
                transaction.hmset(
                    self._make_dialog_key(dialog.dialog_id),
                    'dialog_created_datetime',
                    dialog.dialog_created_datetime.timestamp(),
                )
                transaction.hmset(
                    self._make_dialog_key(dialog.dialog_id),
                    'dialog_id',
                    dialog.dialog_id,
                )

                changed_dialog: entities.QueueDialog = copy.copy(dialog)
                changed_dialog.in_work = False
                changed_dialog.private_user_id = None
                changed_dialogs.append(changed_dialog)

            await transaction.execute()
            return changed_dialogs

    @redis_reconnect
    async def fetch_operator_status(self, operator_id: str) -> int:
        """Fetch operator status."""
        return int(await self.connection.get(self._make_operator_status_key(operator_id), encoding='utf-8'))

    @redis_reconnect
    async def fetch_all_operators(self) -> typing.List[operator_models.Operator]:
        """Fetch all operators statuses."""
        operators_keys: typing.List[str] = []
        cursor: int = 0
        while True:
            cursor, keys = await self.connection.scan(match=self._make_operator_status_global_key(), cursor=cursor)
            operators_keys.extend([key.decode('utf-8') for key in keys])
            if cursor == 0:
                break

        pipeline: Pipeline = self.connection.pipeline()
        for operator_key in operators_keys:
            pipeline.get(operator_key, encoding='utf-8')
            pipeline.zcard(self._make_operator_queue_key(self._extract_operator_id_from_status_key(operator_key)))
        raw_pipeline_result: typing.List[str] = await pipeline.execute()

        operators: typing.List[operator_models.Operator] = []
        for index, operator_key in enumerate(operators_keys):
            operator_id: str = self._extract_operator_id_from_status_key(operator_key)
            operator_status: int = int(raw_pipeline_result[2 * index])
            operator_dialogs_amount: int = int(raw_pipeline_result[2 * index + 1])
            operators.append(
                operator_models.Operator(
                    id=operator_id, current_status=operator_status, dialogs_amount=operator_dialogs_amount
                )
            )

        return operators

    @redis_reconnect
    async def add_operator_status(self, operator_id: str, status: int) -> None:
        """Add operator status."""
        return await self.connection.set(self._make_operator_status_key(operator_id), status)

    @redis_reconnect
    async def fetch_amount_dialogs_by_operator(self, operator_id: str) -> int:
        """Fetch amount dialogs by operator."""
        return await self.connection.zcard(self._make_operator_queue_key(operator_id))

    @redis_reconnect
    async def _fetch_all_dialogs_by_queue_key(self, queue_key: str) -> typing.List[entities.QueueDialog]:
        dialog_ids: list = await self.connection.zrangebyscore(queue_key, encoding='utf-8')
        return await self.fetch_all_dialogs_by_id_list(dialog_ids)

    async def fetch_all_general_queue_dialogs(self) -> typing.List[entities.QueueDialog]:
        """Fetch all dialogs from general queue."""
        return await self._fetch_all_dialogs_by_queue_key(settings.REDIS_GENERAL_QUEUE_KEY)

    async def fetch_all_personal_queue_dialogs(self, operator_id: str) -> typing.List[entities.QueueDialog]:
        """Fetch all dialogs from personal queue."""
        return await self._fetch_all_dialogs_by_queue_key(self._make_operator_queue_key(operator_id))

    async def fetch_all_dialogs_by_id_list(self, dialog_id_list: typing.List[int]) -> typing.List[entities.QueueDialog]:
        """Fetch all dialogs by id list."""
        pipeline: Pipeline = self.connection.pipeline()
        for dialog_id in dialog_id_list:
            pipeline.hgetall(self._make_dialog_key(dialog_id), encoding='utf-8')
        dialog_dicts: list = await pipeline.execute()

        queue_dialogs: typing.List[entities.QueueDialog] = []
        for dialog_dict in dialog_dicts:
            public_user_info: entities.AuthorInfo = entities.AuthorInfo.parse_obj(
                self._pop_dict_item_as_json('public_user_info', dialog_dict)
            )
            queue_dialogs.append(
                entities.QueueDialog(
                    **self._convert_redis_dict_to_python(dialog_dict),
                    public_user_info=public_user_info,
                )
            )

        return queue_dialogs
